import { CakeInterface } from "../types/types";
import { model, Schema } from "mongoose";

const cakeSchema: Schema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        forPersons: {
            type: Number,
            required: true,
        },
        ingredients: {
            type: Array,
            required: true,
        },
    },
    { timestamps: true }
);

export default model<CakeInterface>("Cake", cakeSchema);