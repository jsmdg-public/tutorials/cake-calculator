import mongoose from "mongoose";
import * as server from "./server";
import * as database from "./config/database";

const PORT: string | number = process.env.PORT || 4000;

const app = server.create();

const boot = (uri: string, retries: number) => {
    mongoose
        .connect(uri)
        .then(() =>
            app.listen(PORT, () =>
                console.log(`Server running on http://localhost:${PORT}`)
            )
        )
        .catch((error) => {
            console.log(error);
            if (retries > 0) {
                setTimeout(() => boot(uri, retries - 1), 2000);
                return;
            }
            throw (new Error('could not connect to DB.'));
        });
}

boot(database.getUri(), 5);
