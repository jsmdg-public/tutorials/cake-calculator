import { Response, Request } from "express";
import { CakeInterface } from "../types/types";
import Cake from "../models/cake";

const getAllCakes = async (req: Request, res: Response): Promise<void> => {
    try {
        const menus: CakeInterface[] = await Cake.find();
        await res.status(200).json({ menus });
    } catch (error) {
        throw error;
    }
};

const getIngredientsForCakeAndPersons = async (req: Request, res: Response, next: () => void): Promise<void> => {
    try {
        const {
            body: { name, persons },
        } = req;
        const cake: CakeInterface | null = await Cake.findOne({ name });
        if (!cake) {
            res.status(404).json({ error: `no cake found by name ${name}` })
            return;
        }
    
        const relation = parseInt(persons) / cake.forPersons;
        const result = cake.ingredients.map(ingredient => ({
            ...ingredient,
            amount: ingredient.amount * relation,
        }));
        res.status(200).json({ result });
    } catch (error) {
        throw error;
    }
};

const addCake = async (req: Request, res: Response): Promise<void> => {
    try {
        const body = req.body as Pick<CakeInterface, "name" | "forPersons" | "ingredients">;
        const cake: CakeInterface = new Cake({
            name: body.name,
            forPersons: body.forPersons,
            ingredients: body.ingredients,
        });

        const newCake: CakeInterface = await cake.save();

        res.status(201).json(newCake);
    } catch (error) {
        throw error;
    }
};

const deleteCake = async () => {

};

export { getAllCakes, addCake, getIngredientsForCakeAndPersons, deleteCake }