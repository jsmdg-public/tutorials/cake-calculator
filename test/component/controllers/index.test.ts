import mongoose from "mongoose";
import supertest from "supertest";
import * as server from "../../../src/server";
import Cake from "../../../src/models/cake";
import * as database from "../../../src/config/database";
import { CakeInterface, Units } from "../../../src/types/types";

describe("Test endpoints", () => {

    const app = server.create()

    test("GET /cakes", async () => {
        const cake = await Cake.create({
            name: "Cake 1",
            forPersons: 2,
            ingredients: [
                {
                    name: 'Flour',
                    amount: 500,
                    unit: 'gram',
                },
                {
                    name: 'Butter',
                    amount: 250,
                    unit: 'gram',
                }
            ]
        })

        await supertest(app)
            .get("/cakes")
            .expect(200)
            .then((response) => {
                const contents = response.body
                expect(contents).toHaveProperty("menus")
                expect(Array.isArray(response.body.menus)).toBeTruthy()
                expect(contents.menus).toHaveLength(1)
                expect(contents.menus[0]._id).toBe(cake.id)
                expect(contents.menus[0].name).toBe(cake.name)
                expect(contents.menus[0].ingredients).toHaveLength(2)
            })
    })

    test("POST /cake", async () => {
        const newCake: Pick<CakeInterface, "name" | "forPersons" | "ingredients"> = {
            name: "Lava Cake",
            forPersons: 3,
            ingredients: [
                {
                    name: 'Flour',
                    amount: 500,
                    unit: Units.gram,
                },
                {
                    name: 'Butter',
                    amount: 250,
                    unit: Units.gram,
                },
                {
                    name: 'Chocolate',
                    amount: 150,
                    unit: Units.gram,
                }
            ]
        }

        await supertest(app)
            .post("/cake")
            .expect(201)
            .send(newCake)
            .set('Content-Type', 'application/json')
            .then(async (response) => {
                const cake: CakeInterface | null = await Cake.findOne({ name: "Lava Cake" })
                expect(newCake.name).toBe(cake?.name)
                expect(newCake.ingredients).toHaveLength(3)

            })
    })

    test("POST /ingredients 404", async () => {
        const cake: Pick<CakeInterface, "name" | "forPersons" | "ingredients"> = {
            name: "Invisible Cake",
            forPersons: 1,
            ingredients: []
        }

        await supertest(app)
            .post("/ingredients")
            .expect(404)
            .send(cake)
            .set('Content-Type', 'application/json')
            .then(async (response) => {
                expect(await Cake.findOne({ name: cake.name })).toBeNull()
            })
    })


    test("POST /ingredients", async () => {

        const cake = await Cake.create({
            name: "Salt Water Cake",
            forPersons: 1,
            ingredients: [
                {
                    name: 'Water',
                    amount: 500,
                    unit: Units.millilitre,
                },
                {
                    name: 'Salt',
                    amount: 150,
                    unit: Units.gram,
                },
                {
                    name: 'Sugar',
                    amount: 50,
                    unit: Units.gram,
                }
            ]
        })

        await supertest(app)
            .post("/ingredients")
            .expect(200)
            .send({
                name: cake.name,
                persons: 2
            })
            .set('Content-Type', 'application/json')
            .then(async (response) => {
                const ingredients = response.body.result
                expect(ingredients).toHaveLength(3)
                expect(ingredients[0].amount).toBe(1000)
                expect(ingredients[1].amount).toBe(300)
                expect(ingredients[2].amount).toBe(100)

            })
    })

    test("DELETE /cake/:id", async () => {

        const cake = await Cake.create({
            name: "Deletable Cake",
            forPersons: 1,
            ingredients: [
                {
                    name: 'Rise',
                    amount: 500,
                    unit: Units.millilitre,
                },
                {
                    name: 'Mud',
                    amount: 150,
                    unit: Units.gram,
                }
            ]
        })

        await supertest(app)
            .delete("/cake/" + cake._id)
            .expect(204)
            .set('Content-Type', 'application/json')
            .then(async (response) => {
                expect(await Cake.findOne({ _id: cake._id })).toBeNull()
            })
    })
});

beforeEach((done) => {
    mongoose.connect(database.getUri(), () => done())
})

afterEach((done) => {
    mongoose.connection.db.dropDatabase(() => {
        mongoose.connection.close(() => done())
    })
})
