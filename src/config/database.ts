
function getUri(): string {
    return `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:27017/${process.env.MONGO_DB}?retryWrites=true&w=majority`;
}

export { getUri };