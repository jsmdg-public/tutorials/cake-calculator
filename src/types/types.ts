import { Document } from "mongoose";

export enum Units {
    gram = 'gram',
    millilitre = 'millilitre',
    teaspoon = 'teaspoon',
    tablespoon = 'tablespoon',
    cup = 'cup',
    piece = 'piece'
}

export interface Ingredient {
    name: string;
    amount: number;
    unit: Units
}

export interface CakeInterface extends Document {
    name: string;
    forPersons: number;
    ingredients: Ingredient[];
}