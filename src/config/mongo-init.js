const mongo = new Mongo();
db = mongo.getDB("cake-calculator");
db.createUser(
    {
        user: "cake_admin",
        pwd: "itsACake!",
        roles: [
            {
                role: "readWrite",
                db: "cake-calculator"
            },
            {
                role: "readWrite",
                db: "cake-calculator-test"
            }
        ]
    }
);
db.createCollection('cakes');
db.cakes.insert([
    {
        name: "strawberry-cake",
        forPersons: 10,
        ingredients: [
            {
                name: 'Flour',
                amount: 500,
                unit: 'gram',
            },
            {
                name: 'Butter',
                amount: 250,
                unit: 'gram',
            },
            {
                name: 'sugar',
                amount: 2,
                unit: 'tablespoon',
            },
            {
                name: 'strawberries',
                amount: 750,
                unit: 'gram',
            },
        ]
    },
    {
        name: "chocolate cookies",
        forPersons: 4,
        ingredients: [
            {
                name: 'Flour',
                amount: 250,
                unit: 'gram',
            },
            {
                name: 'Butter',
                amount: 150,
                unit: 'gram',
            },
            {
                name: 'sugar',
                amount: 1,
                unit: 'tablespoon',
            },
            {
                name: 'chocolate cuverture',
                amount: 300,
                unit: 'gram',
            },
        ]
    },
    {
        name: "espresso chocolate chip cake",
        forPersons: 10,
        ingredients: [
            {
                name: 'Flour',
                amount: 3,
                unit: 'cup',
            },
            {
                name: 'baking powder',
                amount: 2,
                unit: 'teaspoon',
            },
            {
                name: 'baking soda',
                amount: 2,
                unit: 'teaspoon',
            },
            {
                name: 'salt',
                amount: 1,
                unit: 'teaspoon',
            },
            {
                name: 'butter',
                amount: 170,
                unit: 'gram',
            },
            {
                name: 'esspresso powder',
                amount: 5,
                unit: 'teaspoon',
            },
            {
                name: 'sugar',
                amount: 350,
                unit: 'gram',
            },
            {
                name: 'egg',
                amount: 4,
                unit: 'pieces',
            },
            {
                name: 'sour cream',
                amount: 120,
                unit: 'gram',
            },
            {
                name: 'milk',
                amount: 160,
                unit: 'millilitre',
            },
            {
                name: 'mini chocolate chips',
                amount: 225,
                unit: 'gram',
            },
        ]
    }

]);
