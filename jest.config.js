module.exports = {
    moduleFileExtensions: ['js', 'json', 'ts'],
    testRegex: '\\.(component-)?test\\.ts$',
    transform: {
        '^.+\\.(t|j)s$': 'ts-jest',
    },
    moduleDirectories: ['node_modules', '<rootDir>'],
    coverageDirectory: 'coverage',
    collectCoverageFrom: ['src/**/*.ts'],
    coveragePathIgnorePatterns: [
        'src/config/mongo-init.js',
    ],
    coverageThreshold: {
        global: {
            branches: 87.5,
            functions: 90,
            lines: 90,
        },
    }
};
