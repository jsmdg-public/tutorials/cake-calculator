# **Coding Task:** Cake Calculator

This API exposes endpoints that allow managing cake recipes (create, read, delete) and the calculation of ingredient's amounts for a given number of persons. 

It consists of 4 endpoints:

- Fetch all cakes.
- Fetch cake's ingredients and amounts depending on the number of people.
- Add a new cake recipe.
- Delete a cake.

## How To Run

This application requires [Docker Desktop](https://docs.docker.com/desktop/mac/install/) or similar solution to be installed in the host machine that enables running docker containers with [docker-compose](https://docs.docker.com/compose/).

Clone this repository and run `docker-compose up`. You might want to run it with the `-d` (detached mode) option that will allow you to run containers in the background. 

The application comes with a [MongoDB](https://www.mongodb.com/docs/manual/tutorial/getting-started/) instance prefilled with data to work with.

## Your Task

Unfortunately, our CTO was messing with the code and it does not look very pretty. 
He has introduced several bad practices due the way the code is written, can you spot those and fix them?
Additionally, some tests are failing, can you fix the code in order to satisfy all test cases?

You can use [insomnia](https://insomnia.rest/download) to interact with the API.

### Tips

- Keep the Git history clean, that is every commit contains self-contained incremental value.
- You can have a document keeping track of the changes you did and why.