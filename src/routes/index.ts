import { Router } from "express";
import {
    getAllCakes,
    addCake,
    getIngredientsForCakeAndPersons,
    deleteCake,
} from "../controllers";

const routes: Router = Router();

routes.get("/cakes", getAllCakes);
routes.post("/cake", addCake);
routes.post("/ingredients", getIngredientsForCakeAndPersons);
routes.delete("/cake/:id", deleteCake);

export default routes;
